package com.example.ksenia.lesson1app;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE = 1001;
    public static final String RESULT_VALUE = "result value";
    private EditText mEditText;
    private Button mButton;
    private TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initListeners();
    }
    private void initViews(){
        mEditText = findViewById(R.id.editText);
        mButton = findViewById(R.id.button);
        mTextView = findViewById(R.id.text_message);
    }
    private void initListeners(){
        mButton.setOnClickListener(new ButtonClickListener());
    }
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            startActivity(InfoActivity.newIntent(MainActivity.this, mEditText.getText().toString()));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(data!=null && requestCode == REQUEST_CODE){
            mEditText.setText(data.getStringExtra(RESULT_VALUE));
        }
    }
}
