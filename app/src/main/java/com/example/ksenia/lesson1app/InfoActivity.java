package com.example.ksenia.lesson1app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {
    private static final String VALUE = "value";
    private TextView mTextView;
    private Button mButton2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        initViews();
        initListeners();
    }

    private void initViews(){
        mTextView = findViewById(R.id.textView);
        mButton2 = findViewById(R.id.button2);
        mTextView.setText(getIntent().getStringExtra(VALUE));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.RESULT_VALUE, mTextView.getText().toString() + " " + Long.toString(System.currentTimeMillis()));
        setResult(MainActivity.REQUEST_CODE, intent);
    }
    private void initListeners(){
        mButton2.setOnClickListener(new InfoActivity.ButtonClickListener());
    }
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(InfoActivity.this, DetailActivity.class);
            startActivity(intent);
        }
    }

    public static final Intent newIntent(Context context, String text) {
        Intent intent = new Intent(context, InfoActivity.class);
        return intent;
    }
}
